To run the program:

Python 2.7 needs to be installed.

cd to the directory where run.py is located. Then run following command:

python run.py --file "PATH TO LOG FILE" --dataRange "TIME RANGE"

For example:

python run.py --file "testing/logFileMix.txt" --dataRange "2013-09-01 9:00:00 - 17:59:59"

For help run:

python run.py -h

To run the tests run the following command:

python test_reportApp.py
