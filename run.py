#!/usr/bin/env python
# -*- coding: utf-8 -*-

from argparse import ArgumentParser
from datetime import datetime


class ReportApp:
    """Class for creating reports from .log files (on a given format).

    .log file format:

    |timestamp              |url           |userid|
    |2013-09-01 09:00:00UTC |/contact.html |12345 |
    |2013-09-01 09:00:00UTC |/contact.html |12346 |
    |2013-09-01 10:00:00UTC |/contact.html |12345 |
    |2013-09-01 11:00:00UTC |/contact.html |12347 |
    """
    def __init__(self, mode='normal', test_params=None):
        self.parse_arguments(mode, test_params)

    def parse_arguments(self, mode, test_params=None):
        """Parse user input from prompt and assign them as attributes to class.

        The mode and test_params are used for unittesting.

           """

        if mode == 'unittest':
            self.filename = test_params['fileName']
            self.data_range = test_params['dataRange']

        else:
            parser = ArgumentParser()
            parser.add_argument("-f", "--file", dest="filename", help="Path to logfile that should be analyzed.", metavar="FILE")
            parser.add_argument("-d", "--dataRange", dest="dataRange", help='Insert date-range on the following format: "2013-09-01 09:00:00 - 10:59:59"',
                                metavar="date")

            args = parser.parse_args()

            self.filename = args.filename   #  Assign path to class instance
            self.data_range = args.dataRange  #  Assign range to class instance

    def read_log_file(self, filename):
        """Reads the .log file provided by user and returns the content as a dictionary.

        Returns
        -------
        log_dict:

        Has the following format:

        {'url': ['/contact.html', '/contact.html', '/contact.html', '/contact.html'],
         'timestamp': ['2013-09-01 09:00:00UTC', '2013-09-01 09:00:00UTC', '2013-09-01 10:00:00UTC', '2013-09-01 11:00:00UTC'],
          'userid': ['12345', '12346', '12345', '12347']}
        """

        all_lines = []

        try:
            with open(filename, "r") as log_file:
                for i, line in enumerate(log_file):
                    splitted_line = line.split('|')
                    no_space_line = [" ".join(x.split()) for x in splitted_line]    #  Remove unnecessary spaces
                    no_space_line = [x for x in no_space_line if len(x) > 0]
                    all_lines.append(no_space_line)

            log_file.close()
            log_dict = {z[0]: list(z[1:]) for z in list(zip(*all_lines))}

        except:
            raise ValueError('The file was not found or is not written on the right format! Please check filepath!')

        return log_dict

    def convert_timestamps(self, log_dict):
        """Converts the timestamps (from strings to datetime objects) in the dictionary containing information form the .log file.

        It makes it much easier to work with the timestamp data.

        Returns
        -------
        log_dict:

        Has the following format:

        {'url': ['/contact.html', '/contact.html', '/contact.html']],
        'timestamp': [datetime.datetime(2013, 9, 1, 9, 0), datetime.datetime(2013, 9, 1, 9, 0), datetime.datetime(2013, 9, 1, 10, 0)],
        'userid': ['12345', '12346', '12345', '12347']}
        """

        timestamps = []
        try:
            for timestamp in log_dict['timestamp']:
                timestamps.append(datetime.strptime(timestamp[:-3], '%Y-%m-%d %H:%M:%S'))

            log_dict['timestamp'] = timestamps
        except:
            raise ValueError('Please check format of timestamps in .log-file!')

        return log_dict

    def filter_data_by_dates(self, date_interval, log_dict):
        """Sort out all data from the log file by using the time limits provided by user in the prompt.

         Returns
        -------
        urls_in_interval: list with all the urls (from .log dict) that have been visited in the given time frame.

        ids_in_interval: list with all the userids (from .log dict) that have been visiting the website in the given time frame..

        """

        date_input = date_interval.split(' ')[0]    #  Extract date from time range
        time_input1 = date_interval.split(' ')[1]   #  Extract lower time limit from time range
        time_input2 = date_interval.split(' ')[3]   #  Extract upper time limit from time range

        limit1 = datetime.strptime(date_input + ' ' + time_input1, '%Y-%m-%d %H:%M:%S')  # Convert strings to datetime objects
        limit2 = datetime.strptime(date_input + ' ' + time_input2, '%Y-%m-%d %H:%M:%S')

        time_stamps_in_interval, urls_in_interval, ids_in_interval = [], [], []

        #  Assemble the data that should be filtered out

        for i, timestamp in enumerate(log_dict['timestamp']):

            if limit1 <= timestamp <= limit2:
                time_stamps_in_interval.append(timestamp)
                urls_in_interval.append(log_dict['url'][i])
                ids_in_interval.append(log_dict['userid'][i])

        return urls_in_interval, ids_in_interval

    def create_report_data(self, urls, ids):
        """Create data on the format that should be printed in the prompt.

         Returns
        -------
        report_data_dict:

        Has the following format:

        {'/contact.html_visitors': 1, '/contact.html': ['12345', '12345'], '/about.html': ['12345'], '/about.html_visitors': 1}

        longest_url: Integer (Needed for better looking printing in the print_report_data method.)

        """

        report_data_dict = {}
        longest_url = 0

        for i, url in enumerate(urls):

            #  Get all user ids for all urls.
            if url not in report_data_dict.keys():
                report_data_dict[url] = []
                report_data_dict[url].append(ids[i])
            else:
                report_data_dict[url].append(ids[i])

            # calculate longest url (for better looking printing later on)
            if len(url) > longest_url:
                longest_url = len(url)

        # calculate number of unique visitors
        for url in report_data_dict.keys():
            report_data_dict[url + '_visitors'] = len(set(report_data_dict[url]))

        return report_data_dict, longest_url

    def print_report_data(self, report_data, longest_url):
        """Prints the report to the prompt."""

        print '|url' + (longest_url - 2) * ' ' + '|page views |visitors |'

        for key in report_data.keys():

            if '_visitors' not in key:
                print '|' + key + (longest_url - len(key)) * ' ' + ' |' + str(len(report_data[key])) + ' ' * 10 + '|' + str(
                    report_data[key + '_visitors']) + ' ' * 8 + '|'

def main():
    """Runs report program."""

    report_app = ReportApp()  # Create object

    log_dict = report_app.read_log_file(report_app.filename)  # Create dicitionary from .log-file

    log_dict = report_app.convert_timestamps(log_dict)  # Convert time data to datetime objects

    urls, ids = report_app.filter_data_by_dates(report_app.data_range, log_dict)  # Get visited urls and userids

    report_data, longest_url = report_app.create_report_data(urls, ids)  # Get data that should be printed out

    report_app.print_report_data(report_data, longest_url)  #  print data to prompt

if __name__ == '__main__':
    main()  # Run program
