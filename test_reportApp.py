#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import run
from datetime import datetime


class TestReportApp(unittest.TestCase):
    """Class for testing ReportApp in run.py
        """
    def setUp(self):
        """Sets up test instances and log_dictionary before each test.
        """

        self.report_app = run.ReportApp(mode='unittest', test_params={'fileName': 'testing/logFileMix.txt', 'dataRange': "2013-09-01 09:00:00 - 10:59:59"})


        self.report_app_wrong_path = run.ReportApp(mode='unittest', test_params={'fileName': 'wrongFileName.txt',
                                                                      'dataRange': "2013-09-01 09:00:00 - 10:59:59"})


        self.log_dict = {'url': ['/contact.html', '/contact.html', '/about.html', '/about.html', '/contact.html', '/home.html', '/about.html',
                                     '/contact.html','hubabajsda'],
                            'timestamp': ['2013-09-01 09:00:00UTC', '2013-09-01 10:00:00UTC', '2013-09-01 11:00:00UTC', '2013-09-01 12:00:00UTC',
                                         '2013-09-01 13:00:00UTC', '2013-09-01 14:00:00UTC', '2013-09-01 15:00:00UTC', '2013-09-02 16:00:00UTC',
                                         '2013-09-02 17:00:00UTC'],
                            'userid': ['12345', '12346', '123sdf45', '123sdf47', '123sdsfr45', '12aasd346', '12345', '12345', '12345']}

    def test_read_log_file(self):
        """Test return value and error handling"""

        #  Check that the returned dictionary is correct.
        self.assertDictEqual(self.report_app.read_log_file(self.report_app.filename), self.log_dict)

        #  Check error handling

        with self.assertRaises(ValueError):
            self.report_app_wrong_path.read_log_file(self.report_app_wrong_path.filename)

    def test_convert_timestamps(self):
        """Test return value and error handling"""

        #  Check that the returned dictionary is correct.
        self.assertDictEqual(self.report_app.convert_timestamps(self.log_dict),
                             {'url': ['/contact.html', '/contact.html', '/about.html', '/about.html', '/contact.html', '/home.html', '/about.html',
                                      '/contact.html', 'hubabajsda'],
                             'timestamp': [datetime(2013, 9, 1, 9, 0), datetime(2013, 9, 1, 10, 0), datetime(2013, 9, 1, 11, 0), datetime(2013, 9, 1, 12, 0),
                                           datetime(2013, 9, 1, 13, 0), datetime(2013, 9, 1, 14, 0),datetime(2013, 9, 1, 15, 0), datetime(2013, 9, 2, 16, 0),
                                           datetime(2013, 9, 2, 17, 0)],
                             'userid': ['12345', '12346', '123sdf45', '123sdf47','123sdsfr45', '12aasd346','12345', '12345', '12345']})

        #  Check error handling

        broken_log_dict = {'timestamp': ['2013-09-01 09:00 :00UTC', '2013-09-01 09: 00:00UTC']}

        with self.assertRaises(ValueError):
            self.report_app.convert_timestamps(broken_log_dict)

    def test_filter_data_by_dates(self):
        """Test return value"""
        date_interval = "2013-09-01 09:00:00 - 17:59:59"

        log_dict_after_conversion = {'url': ['/contact.html', '/contact.html', '/about.html', '/about.html', '/contact.html', '/home.html', '/about.html',
                                                  '/contact.html', 'hubabajsda'], 'timestamp': [datetime(2013, 9, 1, 9, 0), datetime(2013, 9, 1, 10, 0),
                                                                                                datetime(2013, 9, 1, 11, 0), datetime(2013, 9, 1, 12, 0),
                                                                                                datetime(2013, 9, 1, 13, 0), datetime(2013, 9, 1, 14, 0),
                                                                                                datetime(2013, 9, 1, 15, 0), datetime(2013, 9, 2, 16, 0),
                                                                                                datetime(2013, 9, 2, 17, 0)], 'userid': ['12345', '12346',
                                                                                                                                         '123sdf45', '123sdf47',
                                                                                                                                         '123sdsfr45',
                                                                                                                                         '12aasd346', '12345',
                                                                                                                               '12345', '12345']}

        urls, ids = self.report_app.filter_data_by_dates(date_interval, log_dict_after_conversion)

        #  Check that the returned lists are correct
        self.assertListEqual(urls,['/contact.html', '/contact.html', '/about.html', '/about.html', '/contact.html', '/home.html', '/about.html'])
        self.assertListEqual(ids,['12345', '12346', '123sdf45', '123sdf47', '123sdsfr45', '12aasd346', '12345'])

    def test_create_report_data(self):
        """Test return value and error handling"""

        urls = ['/contact.html', '/contact.html', '/about.html', '/about.html', '/contact.html', '/home.html', '/about.html']
        ids = ['12345', '12346', '123sdf45', '123sdf47', '123sdsfr45', '12aasd346', '12345']

        report_data, longest_url = self.report_app.create_report_data(urls, ids)

        #  Check that the returned dictionary is correct.
        self.assertDictEqual(report_data, {'/about.html_visitors': 3,
                                           '/home.html': ['12aasd346'],
                                           '/contact.html': ['12345', '12346', '123sdsfr45'],
                                           '/contact.html_visitors': 3,
                                           '/home.html_visitors': 1,
                                           '/about.html': ['123sdf45', '123sdf47', '12345']} )

        #  Check that the returned integer is correct.
        self.assertEqual(longest_url, 13)

if __name__ == '__main__':
    unittest.main()  # Run all tests